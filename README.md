# Log Presenter

The application parse logs into the meaningful objects and present data from it.

## Usage

```sh
# Parse specific filename(filename with file extension) and define print count of top highest average durations

node build/app.js --file <filename> --top <topNHighestAVGDurations>
#or
node build/app.js -f <filename> -t <topNHighestAVGDurations>

# Help
node build/app.js -h
````

## Example
```sh
node build/app.js -f timing.log -t 5
node build/app.js --file timing.log --top 5
```

![Screenshot](screenshot.png)