"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var lineRgxElement = {
  dateAndTime: /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}/,
  threadId: /\(([^)]+)\)/,
  userContext: /\[(.*?)\]/,
  url: /\/(.*?) in/,
  duration: /\sin (.*?)$/,
  source: /\] ([^/].*?)\s/,
  payload: /\] [^/].* (.*?) in\s/,
  actionQueryString: /action=([^&]*)/
};
var _default = lineRgxElement;
exports["default"] = _default;