"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _commander = _interopRequireDefault(require("commander"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_commander["default"].option('-f, --file <filename>', 'Log filename with file extension', 'timing.log');

_commander["default"].option('-t, --top <number>', 'Print number of highest average request duration', 10);

_commander["default"].parse(process.argv);

var _default = _commander["default"];
exports["default"] = _default;