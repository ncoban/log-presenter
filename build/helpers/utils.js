"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.printHourlyRequestCounts = exports.printHighestDurations = exports.getUniqueResource = exports.convertLineToObject = void 0;

var _cliTable = _interopRequireDefault(require("cli-table"));

var _bars = _interopRequireDefault(require("bars"));

var _regex = _interopRequireDefault(require("../configs/regex"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * The function checks is the URL has "Action=" query string.
 *
 * @param {string} Url - The long URL string
 * @return {boolean}
 */
var isUrlHasActionQueryString = function isUrlHasActionQueryString(url) {
  return url.indexOf('action=') >= 0;
};
/**
 * The function gets the "Action" query string by URL path.
 *
 * @param {string} Url - The long URL string
 * @return {string} Action query string with question mark
 */


var getActionQueryString = function getActionQueryString(url) {
  return "?".concat(url.match(_regex["default"].actionQueryString)[0]);
};
/**
 * The function gets base URL by removing all query strings of URL
 *
 * @param {string} Url - The long URL string
 * @return {string} Base URL string
 */


var getBaseUrl = function getBaseUrl(url) {
  return url.indexOf('?') >= 0 ? url.substring(0, url.indexOf('?')) : url;
};
/**
 * The function parses single line of log file to useable object
 *
 * @param {string} Line - Single line of Log File
 * @return {false} The log line has not any value
 * @return {LogLine} Single line of log object
 */


var convertLineToObject = function convertLineToObject(line) {
  if (!line) {
    return false;
  }

  var lineObject = {
    dateAndTime: line.match(_regex["default"].dateAndTime)[0],
    // threadId: line.match(lineRgxElement.threadId)[1],
    // userContext: line.match(lineRgxElement.userContext)[1],
    duration: parseInt(line.match(_regex["default"].duration)[1], 10)
  };

  if (line.match(_regex["default"].payload)) {
    // lineObject.source = line.match(lineRgxElement.source)[1];
    // lineObject.payload = line.match(lineRgxElement.payload)[1];
    var resource = line.match(_regex["default"].source)[1];
    lineObject.resource = resource;
  } else {
    // lineObject.url = line.match(lineRgxElement.url)[1];
    var _resource = line.match(_regex["default"].url)[1];
    lineObject.resource = _resource;
  }

  return lineObject;
};
/**
 * The function cleans all query strings except "Action" query string to generate unique resource
 *
 * @param {LogLine} Line - Log object of single log line
 * @return {string} The unique resource
 */


exports.convertLineToObject = convertLineToObject;

var getUniqueResource = function getUniqueResource(logObject) {
  var resource = logObject.resource;

  if (isUrlHasActionQueryString(resource)) {
    return getBaseUrl(resource) + getActionQueryString(resource);
  }

  return getBaseUrl(resource);
};
/**
 * The function prints top highest average durations
 *
 * @param {Object} requestGroup - Objects of request groups
 * @param {number} length - Print count of top highest average durations
 */


exports.getUniqueResource = getUniqueResource;

var printHighestDurations = function printHighestDurations(requestGroup, length) {
  var table = new _cliTable["default"]({
    head: ['Request', 'Average Duration'],
    colWidths: [52, 20]
  });
  var sortable = [];

  if (requestGroup) {
    Object.keys(requestGroup).forEach(function (request) {
      if (request) {
        sortable.push([request, requestGroup[request].avgDuration]);
      }
    });
    var elements = sortable.sort(function (a, b) {
      return a[1] < b[1] ? 1 : -1;
    }).slice(0, length);
    elements.forEach(function (el) {
      table.push([el[0], "".concat(el[1].toFixed(2), " ms")]);
    });
    console.log(table.toString());
  }
};
/**
 * The function draws histogram of hourly number of requests.
 *
 * @param {Object} requestCountByHour - Objects of hourly number of requests
 */


exports.printHighestDurations = printHighestDurations;

var printHourlyRequestCounts = function printHourlyRequestCounts(requestCountByHour) {
  console.log((0, _bars["default"])(requestCountByHour, {
    bar: '#',
    width: 28
  }));
};

exports.printHourlyRequestCounts = printHourlyRequestCounts;