"use strict";

var _fs = _interopRequireDefault(require("fs"));

var _eventStream = _interopRequireDefault(require("event-stream"));

var _moment = _interopRequireDefault(require("moment"));

var _utils = require("./helpers/utils");

var _command_line = _interopRequireDefault(require("./helpers/command_line"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

console.time('Process completed');
var file = _command_line["default"].file,
    highestAVGDurationLength = _command_line["default"].top;
console.log("File name: ".concat(file, ", Top n Highest AVG Duration: ").concat(highestAVGDurationLength));
var requestGroup = {};
var requestCountByHour = {};
/**
 * The function push unique resource into the requestGroup object
 *
 * @param {LogLine} logObject - Single line of log object
 */

var pushUniqueResource = function pushUniqueResource(logObject) {
  var duration = logObject.duration;

  if (!requestGroup[logObject.uniqueResourceName]) {
    requestGroup[logObject.uniqueResourceName] = {
      length: 1,
      totalDuration: duration,
      avgDuration: duration
    };
  } else {
    var _requestGroup$logObje = requestGroup[logObject.uniqueResourceName],
        length = _requestGroup$logObje.length,
        totalDuration = _requestGroup$logObje.totalDuration;
    var newAVGDuration = (totalDuration + duration) / (length + 1);
    requestGroup[logObject.uniqueResourceName] = {
      length: length + 1,
      totalDuration: totalDuration + duration,
      avgDuration: newAVGDuration
    };
  }
};
/**
 * The function generate and push request time of single log object into requestCountByHour object.
 *
 * @param {LogLine} logObject - Single line of log object
 */


var pushRequestTime = function pushRequestTime(logObject) {
  var dateAndTime = logObject.dateAndTime;
  var reqDateTime = (0, _moment["default"])(dateAndTime);
  var reqDate = reqDateTime.format('MM-DD-YYYY');
  var reqTimeStart = reqDateTime.format('HH');
  var reqTimeEnd = reqDateTime.add(1, 'hour').format('HH');
  var reqHourKey = "".concat(reqDate, " ").concat(reqTimeStart, ":00:00 to ").concat(reqTimeEnd, ":00:00");

  if (!requestCountByHour[reqHourKey]) {
    requestCountByHour[reqHourKey] = 1;
  } else {
    var length = requestCountByHour[reqHourKey];
    requestCountByHour[reqHourKey] = length + 1;
  }
};

var s = _fs["default"].createReadStream(file).pipe(_eventStream["default"].split()).pipe(_eventStream["default"].mapSync(function (line) {
  s.pause();
  var logObject = (0, _utils.convertLineToObject)(line);

  if (logObject) {
    logObject.uniqueResourceName = (0, _utils.getUniqueResource)(logObject);
    pushUniqueResource(logObject);
    pushRequestTime(logObject);
  }

  s.resume();
}).on('error', function (err) {
  console.log('Error while reading file.', err);
}).on('end', function () {
  (0, _utils.printHighestDurations)(requestGroup, highestAVGDurationLength);
  (0, _utils.printHourlyRequestCounts)(requestCountByHour);
  console.timeEnd('Process completed');
}));
/**
 * @typedef {Object} LogLine
 * @property {string} dateAndTime - Date and time of of single log string
 * @property {string} duration - Duration of single log string
 * @property {string} resource - Resource of single log string
*/