const lineRgxElement = {
  dateAndTime: /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}/,
  threadId: /\(([^)]+)\)/,
  userContext: /\[(.*?)\]/,
  url: /\/(.*?) in/,
  duration: /\sin (.*?)$/,
  source: /\] ([^/].*?)\s/,
  payload: /\] [^/].* (.*?) in\s/,
  actionQueryString: /action=([^&]*)/,
};
export default lineRgxElement;
