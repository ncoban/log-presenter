import program from 'commander';

program
  .option('-f, --file <filename>', 'Log filename with file extension', 'timing.log');
program
  .option('-t, --top <number>', 'Print number of highest average request duration', 10);

program.parse(process.argv);

export default program;
