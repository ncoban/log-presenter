import fs from 'fs';
import es from 'event-stream';
import moment from 'moment';
import {
  convertLineToObject, getUniqueResource, printHighestDurations, printHourlyRequestCounts,
} from './helpers/utils';
import program from './helpers/command_line';

console.time('Process completed');
const { file, top: highestAVGDurationLength } = program;
console.log(`File name: ${file}, Top n Highest AVG Duration: ${highestAVGDurationLength}`);

const requestGroup = {};
const requestCountByHour = {};

/**
 * The function push unique resource into the requestGroup object
 *
 * @param {LogLine} logObject - Single line of log object
 */
const pushUniqueResource = (logObject) => {
  const { duration } = logObject;
  if (!requestGroup[logObject.uniqueResourceName]) {
    requestGroup[logObject.uniqueResourceName] = {
      length: 1,
      totalDuration: duration,
      avgDuration: duration,
    };
  } else {
    const { length, totalDuration } = requestGroup[logObject.uniqueResourceName];
    const newAVGDuration = (totalDuration + duration) / (length + 1);
    requestGroup[logObject.uniqueResourceName] = {
      length: length + 1,
      totalDuration: totalDuration + duration,
      avgDuration: newAVGDuration,
    };
  }
};

/**
 * The function generate and push request time of single log object into requestCountByHour object.
 *
 * @param {LogLine} logObject - Single line of log object
 */
const pushRequestTime = (logObject) => {
  const { dateAndTime } = logObject;
  const reqDateTime = moment(dateAndTime);
  const reqDate = reqDateTime.format('MM-DD-YYYY');
  const reqTimeStart = reqDateTime.format('HH');
  const reqTimeEnd = reqDateTime.add(1, 'hour').format('HH');
  const reqHourKey = `${reqDate} ${reqTimeStart}:00:00 to ${reqTimeEnd}:00:00`;
  if (!requestCountByHour[reqHourKey]) {
    requestCountByHour[reqHourKey] = 1;
  } else {
    const length = requestCountByHour[reqHourKey];
    requestCountByHour[reqHourKey] = length + 1;
  }
};

const s = fs.createReadStream(file)
  .pipe(es.split())
  .pipe(es.mapSync((line) => {
    s.pause();
    const logObject = convertLineToObject(line);
    if (logObject) {
      logObject.uniqueResourceName = getUniqueResource(logObject);
      pushUniqueResource(logObject);
      pushRequestTime(logObject);
    }
    s.resume();
  })
    .on('error', (err) => {
      console.log('Error while reading file.', err);
    })
    .on('end', () => {
      printHighestDurations(requestGroup, highestAVGDurationLength);
      printHourlyRequestCounts(requestCountByHour);
      console.timeEnd('Process completed');
    }));

/**
 * @typedef {Object} LogLine
 * @property {string} dateAndTime - Date and time of of single log string
 * @property {string} duration - Duration of single log string
 * @property {string} resource - Resource of single log string
*/
